import tensorflow as tf
from numpy import expand_dims
from sklearn import metrics
import matplotlib.pyplot as plt
from keras.models import Model
from keras.preprocessing.image import image_utils

from neural_network.plots import plot_confusion_matrix


def create_optimizer(optimizer_type='adam', learning_rate=0.001, momentum=0):
    if optimizer_type == 'adam':
        optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    elif optimizer_type == 'sgdm':
        optimizer = tf.keras.optimizers.SGD(learning_rate=learning_rate, momentum=momentum)
    else:
        optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)

    return optimizer


def show_confusion_matrix(predicted, y_test_normalize, cm_labels, normalize=False, title='Confusion Matrix'):
    confusion_matrix = metrics.confusion_matrix(y_true=y_test_normalize, y_pred=predicted)
    plot_confusion_matrix(cm=confusion_matrix, classes=cm_labels, normalize=normalize, title=title)


def show_augmentation_action(model, train_ds):
    # data augmentation example
    data_augmentation_layer = model.layers[0]
    for images, labels in train_ds.take(1):
        plt.figure(figsize=(10, 10))
        first_image = images[0]
        for i in range(9):
            ax = plt.subplot(3, 3, i + 1)
            augmented_image = data_augmentation_layer(tf.expand_dims(first_image, 0), training=True)
            plt.imshow(augmented_image[0].numpy().astype("int32"))
            plt.title(int(labels[i]))
            plt.axis("off")

    plt.show()


def show_conv_filters_and_feature_map(model, image_width, image_height, layer_base=0, layer_1=0, layer_2=1):
    # retrieve weights from the second hidden layer
    pretrained_layer = model.layers[layer_base]

    # plot first few filters in CONV1
    filters = pretrained_layer.layers[layer_1].get_weights()[0]
    f_min, f_max = filters.min(), filters.max()
    filters = (filters - f_min) / (f_max - f_min)
    n_filters, ix = 6, 1
    for i in range(n_filters):
        # get the filter
        f = filters[:, :, :, i]
        # plot each channel separately (we have 3 channels [3, 3, 3])
        for j in range(3):
            # specify subplot and turn of axis
            ax = plt.subplot(n_filters, 3, ix)
            ax.set_xticks([])
            ax.set_yticks([])
            # plot filter channel in grayscale
            plt.imshow(f[:, :, j], cmap='gray')
            ix += 1

    # show the figure
    plt.show()

    # plot first few filters in CONV2
    filters = pretrained_layer.layers[layer_2].get_weights()[0]
    f_min, f_max = filters.min(), filters.max()
    filters = (filters - f_min) / (f_max - f_min)
    n_filters, ix = 6, 1
    for i in range(n_filters):
        # get the filter
        f = filters[:, :, :, i]
        # plot each channel separately (we have 3 channels [3, 3, 3])
        for j in range(3):
            # specify subplot and turn of axis
            ax = plt.subplot(n_filters, 3, ix)
            ax.set_xticks([])
            ax.set_yticks([])
            # plot filter channel in grayscale
            plt.imshow(f[:, :, j], cmap='gray')
            ix += 1

    # show the figure
    plt.show()

    '''
    FEATURE MAP PLOTTING
    '''
    # redefine model to output right after the first hidden layer
    outputs = pretrained_layer.layers[2].output
    model = Model(inputs=pretrained_layer.inputs, outputs=outputs, name="feature_map_model")
    model.summary()
    # load the image with the required shape
    img = img = image_utils.load_img('Dataset_Animals/Cat/1.jpeg',
                                     target_size=(image_width, image_height, 1))
    # convert the image to an array and reshape it to correct shape
    img = image_utils.img_to_array(img)
    img = img.reshape(image_width, image_height, 3)
    # # expand dimensions so that it represents a single 'sample'
    img = expand_dims(img, axis=0)
    # get feature map for first hidden layer
    feature_maps = model.predict(img)
    # plot the output from each block
    square = 8
    for fmap in feature_maps:
        # plot all 64 maps in an 8x8 squares
        ix = 1
        for _ in range(square):
            for _ in range(square):
                # specify subplot and turn of axis
                ax = plt.subplot(square, square, ix)
                ax.set_xticks([])
                ax.set_yticks([])
                # plot filter channel in grayscale
                plt.imshow(fmap[:, :, ix - 1], cmap='gray')
                ix += 1

        # show the figure
        plt.show()


def load_datasets(data_dir, img_width, img_height, batch_size):
    # datasets
    full_dataset = tf.keras.utils.image_dataset_from_directory(
        data_dir,
        shuffle=True,
        image_size=(img_height, img_width),
        batch_size=batch_size)

    # printing labels
    class_names = full_dataset.class_names
    num_classes = len(class_names)
    print(class_names, " [", num_classes, "]")

    dataset_size = len(full_dataset)
    train_size = int(0.7 * dataset_size)
    val_size = int(0.1 * dataset_size)
    test_size = int(0.2 * dataset_size)

    train_ds = full_dataset.take(train_size)
    full_dataset = full_dataset.skip(train_size)
    val_ds = full_dataset.take(val_size)
    full_dataset = full_dataset.skip(val_size)
    test_ds = full_dataset.take(test_size)

    # autotune data for training
    AUTOTUNE = tf.data.AUTOTUNE
    train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)
    test_ds = test_ds.cache().prefetch(buffer_size=AUTOTUNE)

    return train_ds, val_ds, test_ds, class_names, num_classes



















# def load_dataset(class_name):
#     dataset_jpg = None
#     dataset_jpeg = None
#
#     try:
#         dataset_jpg = load("Dataset_Animals/{}/*.jpg".format(class_name))
#     except Exception:
#         pass
#
#     try:
#         dataset_jpeg = tf.data.Dataset.list_files("Dataset_Animals/{}/*.jpeg".format(class_name))
#     except Exception:
#         pass
#
#     if dataset_jpg is not None and dataset_jpeg is None:
#         return dataset_jpg
#     elif dataset_jpeg is not None and dataset_jpg is None:
#         return dataset_jpeg
#     else:
#         dataset_1: tf.data.Dataset = dataset_jpeg
#         dataset_2: tf.data.Dataset = dataset_jpg
#         return dataset_1.concatenate(dataset_2)

# dataset_cat = load_dataset("Cat").take(1000).shuffle(1000).
#     dataset_chicken = load_dataset("Chicken").take(1000).shuffle(1000)
#     dataset_cow = load_dataset("Cow").take(1000).shuffle(1000)
#     dataset_dog = load_dataset("Dog").take(1000).shuffle(1000)
#     dataset_elephant = load_dataset("Elephant").take(1000).shuffle(1000)
#     dataset_horse = load_dataset("Horse").take(1000).shuffle(1000)
#     dataset_sheep = load_dataset("Sheep").take(1000).shuffle(1000)
#     dataset_squirrel = load_dataset("Squirrel").take(1000).shuffle(1000)
#
#     full_dataset = dataset_cat.concatenate(dataset_chicken).concatenate(dataset_cow).concatenate(dataset_dog) \
#         .concatenate(dataset_elephant).concatenate(dataset_horse).concatenate(dataset_sheep).concatenate(
#         dataset_squirrel).shuffle(8000).batch(batch_size)
