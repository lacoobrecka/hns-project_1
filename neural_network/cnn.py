import numpy as np
import tensorflow as tf
from enum import Enum
from collections import Counter

from keras import activations, Model, Input
from keras.layers import BatchNormalization, Conv2D, MaxPooling2D, AveragePooling2D, concatenate, Flatten, Dense, \
    Dropout, Rescaling, SpatialDropout2D
from keras.optimizers import SGD

from neural_network.utils import create_optimizer


class CnnType(Enum):
    VGG_116 = 0,
    RESNET_50 = 1,
    DENSENET_201 = 2,
    MOBILE_NET = 3,
    CUSTOM_1 = 4,
    CUSTOM_2 = 5


def cnn_create(img_width, img_height, num_classes, augmentation, model_type: CnnType = CnnType.VGG_116):
    # CNN MODEL
    model_cnn = tf.keras.Sequential()

    if augmentation is True:
        # data augmentation layer created
        augmentation_layer = tf.keras.Sequential([
            tf.keras.layers.RandomFlip('horizontal', input_shape=(img_height, img_height, 3)),
            tf.keras.layers.RandomRotation(0.2),
            tf.keras.layers.RandomZoom(0.2)],
            name='data_augmentation')

    # vgg net
    if model_type == CnnType.VGG_116:
        vgg_116_model = tf.keras.applications.VGG16(weights='imagenet', include_top=False,
                                                    input_shape=(img_width, img_height, 3))
        vgg_116_model.trainable = False
        if augmentation is True:
            model_cnn.add(augmentation_layer)

        model_cnn.add(vgg_116_model)
        model_cnn.add(tf.keras.layers.BatchNormalization())
        model_cnn.add(tf.keras.layers.MaxPooling2D())
        model_cnn.add(tf.keras.layers.Flatten())
        model_cnn.add(tf.keras.layers.Dense(512, activation=tf.keras.activations.relu))
        model_cnn.add(tf.keras.layers.Dense(num_classes))

    # resnet
    elif model_type == CnnType.RESNET_50:
        resnet_50_model = tf.keras.applications.ResNet50(weights='imagenet', include_top=False,
                                                         input_shape=(img_height, img_height, 3))
        resnet_50_model.trainable = False
        if augmentation is True:
            model_cnn.add(augmentation_layer)

        model_cnn.add(resnet_50_model)
        model_cnn.add(tf.keras.layers.BatchNormalization())
        model_cnn.add(tf.keras.layers.MaxPooling2D())
        model_cnn.add(tf.keras.layers.Flatten())
        model_cnn.add(tf.keras.layers.Dense(512, activation=tf.keras.activations.relu))
        model_cnn.add(tf.keras.layers.Dense(num_classes))

    # inception net
    elif model_type == CnnType.DENSENET_201:
        densenet_201_model = tf.keras.applications.DenseNet201(weights='imagenet', include_top=False,
                                                               input_shape=(img_height, img_height, 3))
        densenet_201_model.trainable = False
        if augmentation is True:
            model_cnn.add(augmentation_layer)

        model_cnn.add(Rescaling(1. / 255, input_shape=(img_height, img_width, 3)))
        model_cnn.add(densenet_201_model)
        model_cnn.add(tf.keras.layers.MaxPooling2D())
        model_cnn.add(tf.keras.layers.Flatten())
        model_cnn.add(tf.keras.layers.Dense(256, activation='relu'))
        model_cnn.add(tf.keras.layers.Dense(num_classes))

    # mobile net
    elif model_type == CnnType.MOBILE_NET:
        mobile_net_model = tf.keras.applications.MobileNet(weights='imagenet', include_top=False,
                                                           input_shape=(img_height, img_height, 3))
        mobile_net_model.trainable = False
        model_cnn.add(Rescaling(1. / 255, input_shape=(img_height, img_width, 3)))
        model_cnn.add(mobile_net_model)
        model_cnn.add(tf.keras.layers.MaxPooling2D())
        model_cnn.add(Dropout(0.2))
        model_cnn.add(tf.keras.layers.Flatten())
        model_cnn.add(tf.keras.layers.Dense(256, activation='relu'))
        model_cnn.add(tf.keras.layers.Dense(num_classes))

    elif model_type == CnnType.CUSTOM_1:

        if augmentation:
            model_cnn.add(augmentation_layer)

        model_cnn.add(Rescaling(1. / 255, input_shape=(img_height, img_width, 3)))

        model_cnn.add(Conv2D(filters=16, kernel_size=(3, 3), padding='same', activation='relu'))
        model_cnn.add(tf.keras.layers.BatchNormalization())
        model_cnn.add(MaxPooling2D())
        model_cnn.add(SpatialDropout2D(0.2))

        model_cnn.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='relu'))
        model_cnn.add(tf.keras.layers.BatchNormalization())
        model_cnn.add(MaxPooling2D())
        model_cnn.add(SpatialDropout2D(0.3))

        model_cnn.add(Conv2D(filters=64, kernel_size=(3, 3), padding='same', activation='relu'))
        model_cnn.add(tf.keras.layers.BatchNormalization())
        model_cnn.add(MaxPooling2D())
        model_cnn.add(SpatialDropout2D(0.3))

        model_cnn.add(Conv2D(filters=128, kernel_size=(3, 3), padding='same', activation='relu'))
        model_cnn.add(tf.keras.layers.BatchNormalization())
        model_cnn.add(MaxPooling2D())
        model_cnn.add(SpatialDropout2D(0.3))

        model_cnn.add(Conv2D(filters=32, kernel_size=(3, 3), padding='same', activation='relu'))
        model_cnn.add(tf.keras.layers.BatchNormalization())
        model_cnn.add(MaxPooling2D())

        model_cnn.add(tf.keras.layers.Flatten())
        model_cnn.add(tf.keras.layers.Dense(1048, activation='relu'))
        model_cnn.add(Dropout(0.5))
        model_cnn.add(tf.keras.layers.Dense(512, activation='relu'))
        model_cnn.add(Dropout(0.5))
        model_cnn.add(tf.keras.layers.Dense(num_classes, activation=('softmax')))

    elif model_type == CnnType.CUSTOM_2:
        # implement our custom serial-parallel CNN
        # Defining model input
        input_ = Input(shape=(img_height, img_height, 3))
        if augmentation is True:
            input_ = augmentation_layer(input_)

        input_ = Rescaling(1. / 255, input_shape=(img_height, img_width, 3))(input_)
        conv_serial = Conv2D(filters=16, kernel_size=(3, 3), padding='same', activation='relu')(input_)

        # Defining first parallel layer
        in_1 = Conv2D(filters=16, kernel_size=(3, 3), activation='relu', padding='same')(conv_serial)
        conv_1 = BatchNormalization()(in_1)
        conv_1 = AveragePooling2D()(conv_1)

        # Defining second parallel layer
        in_2 = Conv2D(filters=16, kernel_size=(3, 3), activation='relu', padding='same')(conv_serial)
        conv_2 = BatchNormalization()(in_2)
        conv_2 = AveragePooling2D()(conv_2)

        # # Defining third parallel layer
        in_3 = Conv2D(filters=16, kernel_size=(3, 3), activation='relu', padding='same')(conv_serial)
        conv_3 = BatchNormalization()(in_3)
        conv_3 = MaxPooling2D()(conv_3)

        # Concatenating layers
        concat = concatenate([conv_1, conv_2, conv_3])

        concat = Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same')(concat)
        concat = BatchNormalization()(concat)
        concat = MaxPooling2D()(concat)
        concat = SpatialDropout2D(0.3)(concat)

        concat = Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same')(concat)
        concat = BatchNormalization()(concat)
        concat = MaxPooling2D()(concat)

        flat = Flatten()(concat)
        dense = Dense(1048, activation='relu')(flat)
        dense = Dropout(0.3)(dense)
        dense = Dense(512, activation='relu')(dense)
        dense = Dropout(0.3)(dense)
        out = Dense(num_classes, activation='softmax')(dense)

        model_cnn = Model(inputs=[input_], outputs=[out])

    model_cnn.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
                      loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                      metrics=['accuracy'])

    model_cnn.summary()
    return model_cnn


def cnn_train(model, train_ds, val_ds, epochs=10):
    print("\n======== Starting training of model ==========\n")
    early_stopping = tf.keras.callbacks.EarlyStopping(
        monitor="val_loss",
        patience=5
    )
    history = model.fit(
        train_ds,
        validation_data=val_ds,
        epochs=epochs,
        callbacks=[early_stopping]
    )

    return model, history


def cnn_predict(model, test_ds):
    predict = model.predict(test_ds)
    # round predictions
    predict = np.argmax(predict, axis=-1)
    predict_count = Counter(predict)
    predict_count = sorted(predict_count.items())
    return predict, predict_count
