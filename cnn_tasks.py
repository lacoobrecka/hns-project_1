import numpy as np
from tensorflow import keras

from neural_network.cnn import cnn_create, cnn_train, cnn_predict, CnnType
from neural_network.plots import plot_model_accuracy, plot_model_loss
from neural_network.utils import load_datasets, show_confusion_matrix, show_augmentation_action, \
    show_conv_filters_and_feature_map


def cnn_tasks(cnn_type, data_dir, img_width, img_height, batch_size, epochs, augmentation, feature_map):
    # load dataset
    train_ds, val_ds, test_ds, class_names, num_classes \
        = load_datasets(data_dir, img_width, img_height, batch_size)

    # create CNN with pre-trained layer
    model = cnn_create(img_width, img_height, num_classes, augmentation, cnn_type)

    # show example of augmentation pre-processing input
    if augmentation:
        show_augmentation_action(model, train_ds)

    # show some convolution filters and feature map of first convolution layer in pre-trained model
    if feature_map:
        # working only for DENSE NET 201 without augmentation
        assert cnn_type == CnnType.DENSENET_201
        assert augmentation == False
        show_conv_filters_and_feature_map(model, img_width, img_height, 1, 2, 12)

    keras.utils.plot_model(model, to_file='model.png')

    # # train CNN with train and validation dataset
    # model, history = cnn_train(model, train_ds, val_ds, epochs)
    #
    # # results of training
    # results_cnn = model.evaluate(test_ds)
    # print('test loss {}, test accuracy: {} %'.format(results_cnn[0], results_cnn[1] * 100))
    #
    # # plotting of results
    # plot_model_accuracy(history, 'Model Accuracy')
    # plot_model_loss(history, 'Model Loss')
    #
    # # TESTING NETWORKS FOR CONFUSION MATRIX
    # predicted_cnn, predict_count = cnn_predict(model, test_ds)
    # y = np.concatenate([y for x, y in test_ds], axis=0)
    # show_confusion_matrix(predicted_cnn, y, class_names, title='{} CONFUSION MATRIX'
    #                       .format(str(cnn_type).replace('CnnType.', '')))
    # print(predict_count)
