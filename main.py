import tensorflow as tf

from cnn_tasks import cnn_tasks
from neural_network.cnn import CnnType

# parameters
dataset_length = 1000   # for each class
img_height = 180
img_width = 180

epochs = 30       # for pretrained networks 10-15 is enought
batch_size = 50
augmentation = False

feature_map = True
data_dir = "./Dataset_Animals/"

# include GPU
if tf.test.gpu_device_name():
    print('Default GPU Device Details: {}'.format(tf.test.gpu_device_name()))
    physical_devices = tf.config.list_physical_devices("GPU")
    tf.config.experimental.set_memory_growth(physical_devices[0], True)
else:
    print("Please install Tensorflow that supports GPU")

# TODO: MAKE INCEPTION V3, CUSTOM_1 AND CUSTOM_2 TUNING AND TRAINING/TESTING
for i in range(1):
    # this function executes training, evaluating, testing, plotting confusion matrix, etc..
    cnn_tasks(cnn_type=CnnType.DENSENET_201, data_dir=data_dir, img_width=img_width, img_height=img_height,
              batch_size=batch_size, epochs=epochs, augmentation=augmentation,
              feature_map=feature_map)

    print("\n========================\n")
